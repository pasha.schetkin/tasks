<div class="col s12">
    <a href="{{route('tasks.show', ['task' => $task->id])}}">
    <div class="task-card hoverable {{$taskClass()}} center z-depth-2">
        <div class="task-icon">

        </div>
        <div class="task-text">
            <div class="task-title truncate">{{$task->title}}</div>
            <div class="task-timing">{{$task->label}}</div>
            <div class="task-timing">{{$executorName()}}</div>
            <div class="task-info"><span class="task-required">required</span>|<span class="task-status">{{$task->status}}</span></div>
        </div>
    </div>
    </a>
</div>
