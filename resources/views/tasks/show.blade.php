@extends('layouts.app')

@section('content')
    <div class="row">
        <div class="col">
            <h3>Task: <strong>{{$task->title}}</strong></h3>
        </div>
    </div>

    <div class="row">
        <div class="col">
            <p class="justify-content-around">
                {{$task->content ?? 'Нет описания задачи'}}
            </p>
        </div>
    </div>

    <div class="row">
        <div class="col">
            <a href="{{route('tasks.start_execution', ['id' => $task->id])}}" class="btn btn-sm btn-primary">Взять в исполнение</a>
        </div>
    </div>
@endsection
