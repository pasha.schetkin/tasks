@extends('layouts.app')

@section('content')
    <div class="row">
        <div class="col">
            <h3>Tasks</h3>
        </div>
    </div>

    <div style="">
    <div class="row">
        @foreach($tasks as $task)
            <x-task-component :task="$task"></x-task-component>
        @endforeach
    </div>
    </div>

    {{$tasks->links('vendor.pagination.simple-bootstrap-5')}}
@endsection
