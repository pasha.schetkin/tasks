@extends('layouts.app')

@section('content')
    <div class="row">
        <div class="col">
            <h5>Взять в работу задачу: <strong><i>{{$task->title}}</i></strong></h5>
        </div>
    </div>

    <div class="row">
        <div class="col">
            <form action="{{route('tasks.execution', ['id' => $task->id])}}" method="post">
                @csrf
                <div class="mb-3">
                    <label for="exampleFormControlInput1" class="form-label">Планируемое время выполнения</label>
                    <input name="planned_datetime" type="datetime-local" class="form-control" id="exampleFormControlInput1" >
                </div>
                <div class="mb-3">
                    <button class="btn btn-sm btn-primary" type="submit">Взять задачу</button>
                </div>
            </form>
        </div>
    </div>
@endsection
