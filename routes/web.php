<?php

use App\Http\Controllers\TasksController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', [TasksController::class, 'index'])->name('home');
Route::resource('tasks', TasksController::class)->middleware('auth');
Route::get('tasks/start-execution/{id}', [TasksController::class, 'startExecution'])
    ->name('tasks.start_execution')->middleware('auth');
Route::post('tasks/start-execution/{id}', [TasksController::class, 'execution'])
    ->name('tasks.execution')->middleware('auth');
Auth::routes();

