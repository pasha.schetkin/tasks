<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * Class UserTask
 * @package App\Models
 *
 * @property int task_id
 * @property int user_id
 * @property null|Carbon start_datetime
 * @property null|Carbon planned_datetime
 *
 * @property User user
 * @property Task task
 */
class UserTask extends AbstractModel
{
    protected $fillable = ['task_id', 'user_id', 'start_datetime', 'planned_datetime'];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function task()
    {
        return $this->belongsTo(Task::class);
    }
}
