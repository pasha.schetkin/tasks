<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Casts\Attribute;
use Orchid\Filters\Types\Like;
use Orchid\Filters\Types\Where;

/**
 * Class Task
 * @package App\Models
 *
 * @property string title
 * @property null|string content
 * @property int user_id
 * @property int status
 * @property int priority
 * @property string label
 *
 * @property User creator
 */
class Task extends AbstractModel
{
    public const NEW = 'new';
    public const IN_PROGRESS = 'in_progress';
    public const DONE = 'done';

    public const LABELS = [
        self::NEW,
        self::IN_PROGRESS,
        self::DONE
    ];

    public const STATUSES = [
        0 => 'new',
        1 => 'w_i_p',
        2 => 'expired'
    ];

    public const PRIORITIES = [
        0 => 'low',
        1 => 'middle',
        2 => 'highest',
        3 => 'blocker'
    ];

    protected $allowedFilters = [
        'id'         => Where::class,
        'title'      => Like::class,
    ];

    protected $allowedSorts = [
        'id',
    ];

    public function executors()
    {
        return $this->belongsToMany(User::class, 'user_tasks');
    }

    public function executor()
    {
        return $this->belongsToMany(User::class, 'user_tasks')
            ->withPivot(['start_datetime', 'planned_datetime'])
            ->orderByDesc('id')->first();
    }

    public function creator()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }

    public function label(): Attribute
    {
        return Attribute::make(
            get: fn($value) => ucfirst($value)
        );
    }

    public function status(): Attribute
    {
        return Attribute::make(
            get: function(int $status) {
                return self::STATUSES[$status];
            }
        );
    }
}
