<?php

namespace App\View\Components;

use App\Models\Task;
use Closure;
use Illuminate\Contracts\View\View;
use Illuminate\View\Component;

class TaskComponent extends Component
{
    /**
     * Create a new component instance.
     */
    public function __construct(
        public Task $task
    )
    {}

    /**
     * Get the view / contents that represent the component.
     */
    public function render(): View|Closure|string
    {
        return view('components.task-component');
    }

    public function taskClass(): string
    {
        return match ($this->task->getRawOriginal('label')) {
            Task::DONE => 'completed',
            Task::IN_PROGRESS => 'locked',
            default => 'pending'
        };
    }

    public function executorName(): string
    {
        return $this->task->executor()?->name ?? 'Исполнитель не назначен';
    }
}
