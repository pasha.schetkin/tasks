<?php

namespace App\Console\Commands;

use App\Models\Task;
use Illuminate\Console\Command;

class CheckTasksStatusCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'tasks:update-label';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Execute the console command.
     */
    public function handle()
    {
        $tasks = Task::all();
        foreach ($tasks as $task) {
            if (is_null($task->executor()?->pivot->planned_datetime)) {
                $task->status = 0;
                $task->save();
                continue;
            }
            if ($task->executor()?->pivot->planned_datetime >= now()) {
                $task->status = 2;
                $task->save();
            }
        }

        $this->info('Command completed!');
    }
}
