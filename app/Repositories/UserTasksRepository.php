<?php


namespace App\Repositories;


use App\Models\AbstractModel;
use App\Models\UserTask;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Pagination\LengthAwarePaginator;

class UserTasksRepository implements IRepository
{

    public function findById(string $id): null|AbstractModel
    {
        return UserTask::find($id);
    }

    public function getAll(): Collection
    {
        // TODO: Implement getAll() method.
    }

    public function getAllWithPagination(int $perPage = 8): LengthAwarePaginator
    {
        // TODO: Implement getAllWithPagination() method.
    }

    public function store(array $validated): AbstractModel
    {
        return UserTask::create($validated);
    }

    public function update(AbstractModel $model, array $validated): AbstractModel
    {
        // TODO: Implement update() method.
    }

    public function destroy(string $id): bool
    {
        // TODO: Implement destroy() method.
    }
}
