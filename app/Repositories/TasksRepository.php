<?php


namespace App\Repositories;


use App\Models\AbstractModel;
use App\Models\Task;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Pagination\LengthAwarePaginator;

class TasksRepository implements IRepository
{

    public function findById(string $id): null|AbstractModel
    {
        return Task::find($id);
    }

    public function getAll(): Collection
    {
        return Task::all();
    }

    /**
     * @param int $perPage
     * @return LengthAwarePaginator
     */
    public function getAllWithPagination(int $perPage = 8): LengthAwarePaginator
    {
        return Task::paginate($perPage);
    }

    public function store(array $validated): AbstractModel
    {
        return Task::create($validated);
    }

    public function update(AbstractModel $model, array $validated): AbstractModel
    {
        $model->update($validated);
        return $model->fresh();
    }

    public function destroy(string $id): bool
    {
        $model = Task::find($id);
        return $model->delete();
    }
}
