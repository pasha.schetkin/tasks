<?php
namespace App\Repositories;

use App\Models\AbstractModel;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Pagination\LengthAwarePaginator;

interface IRepository
{
    public function findById(string $id): null|AbstractModel;

    public function getAll(): Collection;

    public function getAllWithPagination(int $perPage = 8): LengthAwarePaginator;

    public function store(array $validated): AbstractModel;

    public function update(AbstractModel $model, array $validated): AbstractModel;

    public function destroy(string $id): bool;
}
