<?php

namespace App\Orchid\Layouts\Tasks;

use App\Models\Task;
use Orchid\Screen\Layouts\Table;
use Orchid\Screen\TD;

class ListLayout extends Table
{
    /**
     * Data source.
     *
     * The name of the key to fetch it from the query.
     * The results of which will be elements of the table.
     *
     * @var string
     */
    protected $target = 'tasks';

    /**
     * Get the table cells to be displayed.
     *
     * @return TD[]
     */
    protected function columns(): iterable
    {
        return [
            TD::make('id', 'ID')->sort(),
            TD::make('title', 'Title')->width(350),
            TD::make('content', 'Content')->render(function (Task $task) {
                return $task->content ?? 'Нет описания';
            })->width(350),
            TD::make('user_id', "Creator")->render(function(Task $task) {
                return $task->creator->name;
            })
        ];
    }
}
