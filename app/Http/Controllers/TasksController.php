<?php

namespace App\Http\Controllers;

use App\Repositories\TasksRepository;
use App\Repositories\UserTasksRepository;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Foundation\Application;
use Illuminate\Http\Request;

class TasksController extends Controller
{
    public function __construct(
        public TasksRepository $taskRepository,
        protected UserTasksRepository $userTasksRepository
    )
    {}

    public function index()
    {
        $tasks = $this->taskRepository->getAllWithPagination();
        return view('tasks.index', compact('tasks'));
    }

    public function show(string $id)
    {
        $task = $this->taskRepository->findById($id);
        return view('tasks.show', compact('task'));
    }

    public function startExecution(string $id): View|Application|Factory|\Illuminate\Contracts\Foundation\Application
    {
        $task = $this->taskRepository->findById($id);
        return view('tasks.start_execution', compact('task'));
    }

    public function execution(Request $request, string $id)
    {
        $validated = $request->validate([
            'planned_datetime' => ['nullable', 'after:today']
        ]);
        $this->userTasksRepository->store([
            'user_id' => $request->user()->id,
            'task_id' => $id,
            'start_datetime' => now(),
            'planned_datetime' => $validated['planned_datetime']
        ]);
        return redirect()->route('home');
    }
}
